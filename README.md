# css-attr-polyfill

#### 介绍
Polyfill吊炸天的CSS attr()新语法，取代原子类CSS，即插即用。

目前该项目还属于实验性质，未在生产环境大规模测试，欢迎共同建设。

#### Demo示意

猛击这里：https://zhangxinxu.gitee.io/css-attr-polyfill/css-attr.html

#### 文档
详见这里：https://www.zhangxinxu.com/wordpress/2020/10/css-attr-polyfill/

#### 使用
1. 引入css-attr.js

```html
<script src="./css-attr.js"></script>
```

2. attr()的新语法使用CSS自定义属性表示，例如：

```html
<button bgcolor="skyblue" radius="4">按钮</button>
<button bgcolor="#00000040" radius="1rem">按钮</button>
<button bgcolor="red" radius="50%">按钮</button>
<button bgcolor="orange" radius="100% / 50%">按钮</button>
```

```css
button {
    border: 0;
    padding: .5em 1em;
}
button {
    --attr-bg: attr(bgcolor color);
    background-color: var(--attr-bg);
    --attr-radius: attr(radius px, 4px);
    border-radius: var(--attr-radius);
}
```

效果如下图所示：

<img loading="lazy" src="https://imgservices-1252317822.image.myqcloud.com/image/010620220144528/83338b78.png" width="406" height="61" alt="attr()函数Polyfill后的生效示意">

#### 兼容性

支持CSS变量的浏览器均支持。


#### 许可证

MIT
